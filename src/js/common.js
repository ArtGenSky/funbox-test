$(".js-product-item").change(function() {
	var $this = $(this);
	var $productBlock = $this.closest(".product-item");

	$productBlock.toggleClass("product-item_selected");
});