var gulp = require('gulp'),
	pug = require('gulp-pug'),
	sass = require('gulp-sass'),
	postcss = require('gulp-postcss'),
	autoprefixer = require('autoprefixer'),
	browserSync = require('browser-sync');


var paths = {
    scss: {
    	src: './src/scss/main.scss',
    	dest: './dist/css'
    },
    css: {
    	src: './src/css/**/*.css',
    	dest: './dist/css'
    },
    pug: {
    	src: './src/**/*.pug',
    	dest: './dist'
    },
    js: {
    	src: './src/js/**/*.js',
    	dest: './dist/js'
    },
    fonts: {
    	src: './src/fonts/**/*',
    	dest: './dist/fonts'
    },
    img: {
    	src: './src/img/**/*',
    	dest: './dist/img'
    }
};

gulp.task('pug', function() {
	return gulp.src(paths.pug.src)
		.pipe(pug({
			pretty: '	'
		}))
		.pipe(gulp.dest(paths.pug.dest));
});

var postCSSplugins = [
	autoprefixer({browsers: ['last 5 versions', 'ie 11']})
];

gulp.task('scss', function() {
	return gulp.src(paths.scss.src)
		.pipe(sass({
			outputStyle: 'expanded',
			errLogToConsole: false,
        	onError: function(err) {
            	return notify().write(err);
        	}
		}))
		.pipe(postcss(postCSSplugins))
		.pipe(gulp.dest(paths.scss.dest));
});

gulp.task('styles', function() {
	return gulp.src(paths.css.src)
		.pipe(postcss(postCSSplugins))
		.pipe(gulp.dest(paths.css.dest));
});

gulp.task('scripts', function() {
	return gulp.src(paths.js.src)
		.pipe(gulp.dest(paths.js.dest));
});

gulp.task('fonts', function() {
	return gulp.src(paths.fonts.src)
		.pipe(gulp.dest(paths.fonts.dest));
});

gulp.task('img', function() {
	return gulp.src(paths.img.src)
		.pipe(gulp.dest(paths.img.dest));
});

gulp.task('build', gulp.parallel(
	'pug',
	'scss',
	'styles',
	'scripts',
	'fonts',
	'img'
));

gulp.task('watch:scss', function() {
	return gulp.watch(paths.scss.src, gulp.series('scss'));
});
gulp.task('watch:css', function() {
	return gulp.watch(paths.css.src, gulp.series('styles'));
});
gulp.task('watch:pug', function() {
	return gulp.watch(paths.pug.src, gulp.series('pug'));
});
gulp.task('watch:js', function() {
	return gulp.watch(paths.js.src, gulp.series('scripts'));
});

gulp.task('watch', gulp.parallel(
	'watch:scss',
	'watch:css',
	'watch:pug',
	'watch:js'
));

var browserSyncConfig = {
    server: {
        baseDir: './dist/'
    },
    files: [
        './dist/*.html',
        './dist/css/**/*.css',
        './dist/js/**/*.js'
    ],
    notify: false
};

gulp.task('browserSync', function() {
    browserSync.init(browserSyncConfig);
});

gulp.task('serve', gulp.series('build', gulp.parallel('watch', 'browserSync')));
gulp.task('default', gulp.series('build', gulp.parallel('watch', 'browserSync')));